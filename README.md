# `ptr::Unique` for stable Rust

This project can be used to create your own collections that
might want to use an optimized pointer but doesn't want to 
manually implement `Send` and `Sync`. This project uses the
sources of the internal implementation of `Unique` and a 
`NonNull` pointer to guarrentee null pointer optimization.